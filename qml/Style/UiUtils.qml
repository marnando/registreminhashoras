import QtQuick 2.0

pragma Singleton

QtObject {
    id: uiStyle

    // Margins
    readonly property int margins: 10
    readonly property int marginMedium: 15
    readonly property int marginLarge: 20
    readonly property int marginXLarge: 25

    // Color Scheme
    readonly property color transparent: "transparent"

    // Green
    readonly property color colorQtPrimGreen: "#41cd52"
    readonly property color colorQtAuxGreen1: "#21be2b"
    readonly property color colorQtAuxGreen2: "#17a81a"
    readonly property color colorGreen: "#4CAF50"
    readonly property color colorTeal: "#009688"
    readonly property color colorLightTeal: "#80CBC4"
    readonly property color colorLightGreen: "#8BC34A"
    readonly property color colorLime: "#CDDC39"

    // Gray
    readonly property color colorQtGray1: "#09102b"
    readonly property color colorQtGray2: "#222840"
    readonly property color colorQtGray3: "#3a4055"
    readonly property color colorQtGray4: "#53586b"
    readonly property color colorQtGray5: "#53586b"
    readonly property color colorQtGray6: "#848895"
    readonly property color colorQtGray7: "#9d9faa"
    readonly property color colorQtGray8: "#b5b7bf"
    readonly property color colorQtGray9: "#cecfd5"
    readonly property color colorQtGray10: "#f3f3f4"

    // Blue
    readonly property color colorLighBlue: "#03A9F4"
    readonly property color colorLightSteelBlue: "#b0c4de"

    // Rose
    readonly property color colorPink: "#E91E63"

}
