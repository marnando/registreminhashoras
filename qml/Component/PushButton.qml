import QtQuick 2.0
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

Button {
    id: pushButton
    background: Rectangle {
        radius: 5
        color: pushButton.down ? "purple" : "transparent"
        border.color: "pink"
    }
}
