import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtQuick.Window 2.3
import "qml/Component"
import "qml/Style"

ApplicationWindow {
    id: root
    visible: true
    height: 600
    width: 350
    title: qsTr("Registro de Horas")
    visibility: Qt.platform.os === "android" ? Window.FullScreen : Window.Windowed

    ColumnLayout {
        anchors.fill: parent
        spacing: UiUtils.margins
        SwipeView {
            id: view
            currentIndex: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            Component.onCompleted: {
//                view.interactive = false
            }
            Item {
                id: page1
                ColumnLayout {
                    anchors.fill: parent
                    Column {
                        anchors.centerIn: parent
                        spacing: UiUtils.margins
                        TextField {
                            id: idText
                            width: parent.width
                            placeholderText: "User"

                            Rectangle {
                                id: rectNotification
                                height: parent.height
                                width: 5
//                                color: UiUtils.colorPink
                                color: "pink"
                                visible: false
                                anchors.right: parent.right
                            }

                            onActiveFocusChanged: {
                                if (!focus) {
                                    if (idText.text.length < 1
                                            ? rectNotification.visible = true
                                            : rectNotification.visible = false);
                                }
                            }
                        }

                        TextField {
                            width: parent.width
                            placeholderText: "Pass"
                        }
                        Row {
                            spacing: UiUtils.margins
                            PushButton {
                                text: "Cancelar"
                            }
                            PushButton {
                                text: "Concluir"
                            }
                        }
                    }
                }
            }
            Item {
                id: page2

                Item {
                    id: dockRoundRect
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: rect.top

                    Rectangle {
                        id: roundRect
                        height: (page2.height / 2) * 0.8
                        width: height
                        radius: height / 2
                        color: UiUtils.transparent
                        border.color: UiUtils.colorPink
                        anchors.centerIn: parent
                        anchors.margins: 10

                        ColumnLayout {

                        }
                    }
                }

                Rectangle {
                    id: rect
                    height: parent.height / 2
                    radius: 5
                    color: UiUtils.transparent
                    border.color: UiUtils.colorPink
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: 10
                }
            }
        }
    }
}
