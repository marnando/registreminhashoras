#include "frontcontroller.h"
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

FrontController::FrontController(QObject *parent)
    : QObject(parent)
    , m_engine(new QQmlApplicationEngine(this))
{

}

FrontController::~FrontController()
{

}

int FrontController::initApp()
{
    m_engine->load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (m_engine->rootObjects().isEmpty()) {
        return -1;
    }
    return 0;
}

void FrontController::setAppParameters()
{
    setStyle();
    registerSingletons(QUrl("qrc:/qml/Style/UiUtils.qml"), "UiUtils", 1, 0, "UiUtils");
    setRootContext();
}

void FrontController::setRootContext()
{
    m_engine->rootContext()->setContextProperty("frontController", this);
}

void FrontController::setStyle()
{
    QQuickStyle::setStyle("Material");
}

void FrontController::registerSingletons(const QUrl &url, const char *uri, int versionMajor, int versionMinor, const char *qmlName)
{
    qmlRegisterSingletonType(url, uri, versionMajor, versionMinor, qmlName);
}
