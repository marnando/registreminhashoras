#ifndef FRONTCONTROLLER_H
#define FRONTCONTROLLER_H

#include <QObject>
#include <QQmlApplicationEngine>

class FrontController : public QObject
{
    Q_OBJECT

protected:
    QQmlApplicationEngine * m_engine;

public:
    explicit FrontController(QObject *parent = nullptr);
    ~FrontController();
    int initApp();

private:
    void setAppParameters();
    void setRootContext();
    void setStyle();
    void registerSingletons(const QUrl &url, const char *uri, int versionMajor, int versionMinor, const char *qmlName);

signals:

public slots:
};

#endif // FRONTCONTROLLER_H
