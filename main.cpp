#include <QGuiApplication>
#include "src/control/frontcontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    FrontController controller;
    controller.initApp();
    return app.exec();
}
